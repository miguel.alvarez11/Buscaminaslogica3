package model;

import java.util.Random;

public class MatrizEnTripletas {

    private Tripleta[] vectorTripletas;

    // Metodo Constructor
    public MatrizEnTripletas(Tripleta tripleta) {

        int m = tripleta.getFila();
        int n = tripleta.getColumna();
        int p = m * n + 2;

        vectorTripletas = new Tripleta[p];
        vectorTripletas[0] = tripleta;
        // Crea la matriz con la tripleta que se entrego
        for (int i = 1; i < p; i++) {
            vectorTripletas[i] = null;
        }

    }
    // Cambia el valor de la posicion de tripletas
    public void asignarNumeroTripletas(int n) {
        Tripleta tripleta = vectorTripletas[0];
        tripleta.setValor(n);
        vectorTripletas[0] = tripleta;
    }

    //Getters & Setters
    public int getNumeroFilas() {
        return vectorTripletas[0].getFila();
    }
    public int getNumeroColumnas() {
        return vectorTripletas[0].getColumna();
    }
    public int getNumeroTripletas() {
        return vectorTripletas[0].getValor();
    }
    public Tripleta getTripleta(int i) {
        return vectorTripletas[i];
    }
    public Tripleta getTripleta(int fila, int columna){
        for (int i = 1; i <= getNumeroTripletas(); i++) {
            if (vectorTripletas[i] != null && vectorTripletas[i].getFila() == fila && vectorTripletas[i].getColumna() == columna) {
                return vectorTripletas[i];
            }
        }
        return null;
    }
    // Se encarga de imprimir en consola cada una de las tripletas de la matriz.
    public void mostrarMatrizEnTripletas() {
        int i = 1;
        while (vectorTripletas[i] != null && i <= vectorTripletas[0].getValor()) {
            System.out.println(vectorTripletas[i].getFila() + "," + vectorTripletas[i].getColumna() + "," + vectorTripletas[i].getValor());
            i++;
        }
    }
    // introduce una tripleta a una matriz creada
    public void insertarTripleta(Tripleta ti) {
        int i, j, p;
        Tripleta t, tx;
        tx = getTripleta(0);
        p = (int) tx.getValor();
        i = 1;
        t = getTripleta(i);

        while (i <= p && t.getFila() < ti.getFila()) {
            i++;
            t = getTripleta(i);
        }

        while (i <= p && t.getFila() == ti.getFila()
                && t.getColumna() <= ti.getColumna()) {
            i++;
            t = getTripleta(i);
        }
        p++;
        j = p - 1;
        while (j >= i) {
            vectorTripletas[j + 1] = vectorTripletas[j];
            j--;
        }
        vectorTripletas[i] = ti;
        asignarNumeroTripletas(p);
    }
    // confirma si existe una tripleta en matriz
    public boolean existeTripleta(int fila, int columna) {
        //recorre la matriz y retorna si esta o no la tripleta en ella
        for (int i = 1; i <= getNumeroTripletas(); i++) {
            if (vectorTripletas[i] != null && vectorTripletas[i].getFila() == fila && vectorTripletas[i].getColumna() == columna) {
                return true;
            }
        }
        return false;
    }




    public void sumarValorATripleta(int fila, int columna) {
        for (int i = 1; i <= getNumeroTripletas(); i++) {
            if (vectorTripletas[i] != null && vectorTripletas[i].getFila() == fila && vectorTripletas[i].getColumna() == columna) {

                if (vectorTripletas[i].getValor() != 9) {
                    vectorTripletas[i].setValor(vectorTripletas[i].getValor() + 1);
                } else
                    return;
            }
        }
    }
    // Crea una tripleta con tales valores y la inserta en la matriz
    public void crearTripletaEnPosicion(int fila, int columna) {
        Tripleta tripleta = new Tripleta(fila, columna, 1);
        insertarTripleta(tripleta);
    }

    // Método encargado de repartir aleatoriamente las minas y asignar valores a las casillas.
    public void asignarMinasYValores(int numMinas) {
        int numFila = getNumeroFilas();
        int numColumna = getNumeroColumnas();
        int posFila, posColumna;
        Random random = new Random();

        for (int i = 0; i < numMinas; i++) {
            posFila = random.nextInt(numFila) + 1;
            posColumna = random.nextInt(numColumna) + 1;

            // Verificamos si ya hay una mina en la posición obtenidad
            for (int j = 1; j <= getNumeroTripletas(); j++) {
                if (vectorTripletas[j] != null && vectorTripletas[j].getFila() == posFila && vectorTripletas[j].getColumna() == posColumna
                        && vectorTripletas[j].getValor() == 9) {

                    System.out.println("Ya existe un mina en esta posición.");
                    posFila = random.nextInt(numFila) + 1;
                    posColumna = random.nextInt(numColumna) + 1;
                    j = 1;

                }
            }

            // Creamos la tripleta Mina en la posición aleatoria que obtuvimos
            if (existeTripleta(posFila, posColumna)) {
                for (int j = 1; j <= getNumeroTripletas(); j++) {
                    if (vectorTripletas[j] != null && vectorTripletas[j].getFila() == posFila && vectorTripletas[j].getColumna() == posColumna) {
                        vectorTripletas[j].setValor(9);
                    }
                }
            } else {
                Tripleta mina = new Tripleta(posFila, posColumna, 9);
                insertarTripleta(mina);
            }

            // Verificamos si existen tripletas a su alrededor, de lo contrario, las creamos.
            if (posFila != numFila && existeTripleta(posFila + 1, posColumna)) {
                sumarValorATripleta(posFila + 1, posColumna);
            } else if (posFila != numFila && !existeTripleta(posFila + 1, posColumna)) {
                crearTripletaEnPosicion(posFila + 1, posColumna);
            }

            if (posFila != 1 && existeTripleta(posFila - 1, posColumna)) {
                sumarValorATripleta(posFila - 1, posColumna);
            } else if (posFila != 1 && !existeTripleta(posFila - 1, posColumna)) {
                crearTripletaEnPosicion(posFila - 1, posColumna);
            }

            if (posColumna != numColumna && existeTripleta(posFila, posColumna + 1)) {
                sumarValorATripleta(posFila, posColumna + 1);
            } else if (posColumna != numColumna && !existeTripleta(posFila, posColumna + 1)) {
                crearTripletaEnPosicion(posFila, posColumna + 1);
            }

            if (posColumna != 1 && existeTripleta(posFila, posColumna - 1)) {
                sumarValorATripleta(posFila, posColumna - 1);
            } else if (posColumna != 1 && !existeTripleta(posFila, posColumna - 1)) {
                crearTripletaEnPosicion(posFila, posColumna - 1);
            }

            if (posFila != numFila && posColumna != numColumna && existeTripleta(posFila + 1, posColumna + 1)) {
                sumarValorATripleta(posFila + 1, posColumna + 1);
            } else if (posFila != numFila && posColumna != numColumna && !existeTripleta(posFila + 1, posColumna + 1)) {
                crearTripletaEnPosicion(posFila + 1, posColumna + 1);
            }

            if (posFila != 1 && posColumna != numColumna && existeTripleta(posFila - 1, posColumna + 1)) {
                sumarValorATripleta(posFila - 1, posColumna + 1);
            } else if (posFila != 1 && posColumna != numColumna && !existeTripleta(posFila - 1, posColumna + 1)) {
                crearTripletaEnPosicion(posFila - 1, posColumna + 1);
            }

            if (posFila != numFila && posColumna != 1 && existeTripleta(posFila + 1, posColumna - 1)) {
                sumarValorATripleta(posFila + 1, posColumna - 1);
            } else if (posFila != numFila && posColumna != 1 && !existeTripleta(posFila + 1, posColumna - 1)) {
                crearTripletaEnPosicion(posFila + 1, posColumna - 1);
            }

            if (posFila != 1 && posColumna != 1 && existeTripleta(posFila - 1, posColumna - 1)) {
                sumarValorATripleta(posFila - 1, posColumna - 1);
            } else if (posFila != 1 && posColumna != 1 && !existeTripleta(posFila - 1, posColumna - 1)) {
                crearTripletaEnPosicion(posFila - 1, posColumna - 1);
            }


        }
    }

}
