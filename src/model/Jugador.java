package model;

public class Jugador {
    String nombre = "";
    int puntaje = 0;
    String dificultad = "";
    int tiempoAcumulado = 0;
    // Metodo Constructor
    public Jugador(String nombre, int puntaje, int tiempoAcumulado) {
        this.nombre = nombre;
        this.puntaje = puntaje;
        this.tiempoAcumulado = tiempoAcumulado;
    }

    //Getter & Setters
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getPuntaje() {
        return puntaje;
    }
    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
    public String getDificultad() {
        return dificultad;
    }
    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }
    public int getTiempoAcumulado() {
        return tiempoAcumulado;
    }
    public void setTiempoAcumulado(int tiempoAcumulado) {
        this.tiempoAcumulado = tiempoAcumulado;
    }

}
