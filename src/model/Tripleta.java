package model;

public class Tripleta {

    private int fila, columna;
    private int valor;

    // Metodo constructor
    public Tripleta(int fila, int columna, Integer valor){
        this.fila = fila;
        this.columna = columna;
        this.valor = valor;
    }
    // Getters & Setters
    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

}
