package model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;

public class Model {

    // Clase que se encarga de lo relacionado al tiempo en su totalidad
    public class Timer extends AnchorPane {
        private Label label;

        private Timeline anim;
        private int tmp = 4;
        private String S = "";
        // Constructor
        public Timer(Label label) {
            this.label = label;
            anim = new Timeline(new KeyFrame(Duration.seconds(1), e -> timeLabel()));
            anim.setCycleCount(Timeline.INDEFINITE);
            anim.play();
        }
        // se encargaria de una animacion de el paso del tiempo decreciendo
        private void timeLabel() {
            if (tmp > 0) {
                tmp--;
            } else {
                anim.stop();
                return;
            }
            S = tmp + "";
            label.setText(S);
        }
    }
}



