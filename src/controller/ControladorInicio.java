package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;


import javax.swing.*;
import java.io.IOException;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class ControladorInicio{

    public static String nombre;

    @FXML
    public TextField J1;
    @FXML
    public AnchorPane Anchor;







    // boton que ejecuta la ventana de Elegir modo

    public void btnComoJugar(javafx.event.ActionEvent actionEvent) throws IOException {
        JOptionPane.showMessageDialog(null,"Mini Guia! \n\n -Primero Elige la dificultad = numero de casillas y Minas\n " +
                "-Luego, das click izquierdo en cualquier casilla si esta casilla no tiene ninguna mina en ninguno de los cuadrados cercanos esta quedara vacia\n" +
                "-Si hay 1 mina cerca la casilla tendra un 1, si clickeas una casilla con una Mina perderas el juego!\n" +
                "-Asi que si sospechas de una casilla Mina marcala con el click derecho\n" +
                "-Cuando descubras todas las casillas Ganaras, Recuerda entre mas rapido lo hagas Mejor. Mucha Suerte ;D" );
    }
    // nos lleva a la ventana para Elegir Modo
    public void btnEmpezarJuego(javafx.event.ActionEvent actionEvent) throws IOException {
        nombre = J1.getText();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ElegirModo.fxml"));
        Parent root = loader.load();
        ControladorElegirModo controlador = (ControladorElegirModo) loader.getController();
        Scene ElegirModo = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(ElegirModo);
        stage.setResizable(false);
        stage.show();
    }


    // metodo que ayuda para poder agarrar el nombre de javafx
    public void checkLetters() {
        Pattern pattern = Pattern.compile("[a-zA-Z]*");
        UnaryOperator<TextFormatter.Change> filter = c -> {
            if (pattern.matcher(c.getControlNewText()).matches()) {
                return c;
            } else {
                return null;
            }
        };
        TextFormatter<String> formatter = new TextFormatter<>(filter);
        J1.setTextFormatter(formatter);
    }

}


