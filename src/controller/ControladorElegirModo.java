package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.*;
import java.io.IOException;
import model.MatrizEnTripletas;
import model.Tripleta;


public class ControladorElegirModo extends ControladorInicio {


    static public int numFila, numcolumna;
    static int numMinas;
    static public MatrizEnTripletas matrizEnTripletas;

    @FXML
    public Button btnJM2;

    @FXML
    public Button RB1;
    @FXML
    public Button RB2;
    @FXML
    public Button RB3;
    @FXML
    public Button RB4;




    // metodo para ir a la ventana desarrollo
    private void irDesarrollo() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Desarrollo.fxml"));
        Parent root = loader.load();
        Scene desarrollo = new Scene(root);
        Stage stage1 = new Stage();
        stage1.setScene(desarrollo);
        stage1.show();

    }

    // Botones que asignan la dificultad con las filas columnas y minas predeterminadas, y al final ir a desarollo
    public void btn1(javafx.event.ActionEvent actionEvent) throws IOException {
       numFila = 8;
       numcolumna = 8;
       numMinas = 10;
       Tripleta tripleta = new Tripleta(numFila,numcolumna,0);
       matrizEnTripletas = new MatrizEnTripletas(tripleta);
       matrizEnTripletas.asignarMinasYValores(10);
       matrizEnTripletas.mostrarMatrizEnTripletas();
        irDesarrollo();
    }
    public void btn2(javafx.event.ActionEvent actionEvent) throws IOException {
        numFila = 16;
        numcolumna = 16;
        numMinas = 40;
        Tripleta tripleta = new Tripleta(numFila,numcolumna,0);
        matrizEnTripletas = new MatrizEnTripletas(tripleta);
        matrizEnTripletas.asignarMinasYValores(40);
        matrizEnTripletas.mostrarMatrizEnTripletas();
        irDesarrollo();
    }
    public void btn3(javafx.event.ActionEvent actionEvent) throws IOException {
        numFila = 16;
        numcolumna = 29;
        numMinas = 99;
        Tripleta tripleta = new Tripleta(numFila,numcolumna,0);
        matrizEnTripletas = new MatrizEnTripletas(tripleta);
        matrizEnTripletas.asignarMinasYValores(99);
        matrizEnTripletas.mostrarMatrizEnTripletas();
        irDesarrollo();
    }
    // boton que consigue los valores para crear un buscaminas personalizado
    public void btn4(javafx.event.ActionEvent actionEvent) throws IOException {

        numFila = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de filas. "));
        numcolumna = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de columnas. "));
        numMinas = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de Minas. "));
        Tripleta tripleta = new Tripleta(numFila,numcolumna,0);
        matrizEnTripletas = new MatrizEnTripletas(tripleta);
        matrizEnTripletas.asignarMinasYValores(numMinas);
        // Miramos que las filas y las columnas no sean demasiado grandes
        if(numFila > 21 || numcolumna > 38){
            JOptionPane.showMessageDialog(null, "Valores ingresados no válidos. \nPor favor ingrese otros valores. \n\nNúmero máximo de filas: 21\nNúmero máximo de columnas: 38");
        return;
        }
        irDesarrollo();
    }
    //ejecuta la ventana Final de puntajes


}
