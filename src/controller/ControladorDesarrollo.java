package controller;


import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Jugador;
import model.Model;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ControladorDesarrollo extends ControladorElegirModo {

    private int numFilas, numColumnas;

    List<String> listaCasillasPresionadas;
    List<String> listaCasillasBandera;
    public Model model;
    int numCasillas = numFilas * numColumnas - numMinas - 1;
    public Model.Timer timer;
    public int tiempoCount;
    private static Jugador J1 = new Jugador("x",0,0);

    public static Jugador getJ1() {
        return J1;
    }
    public static void setJ1(Jugador j1) {
        J1 = j1;
    }
    @FXML
    HBox hBox;

    @FXML
    Label labelNombre;

    @FXML
    BorderPane contenedor;

    @FXML
    Pane boxButtons;

    @FXML
    GridPane gPaneMatriz;


    public boolean buscarCasilla(List<String> list, String posicion) {
        numCasillas = numFilas * numColumnas - numMinas;
        for (String pos : list) {
            if (pos.equals(posicion)) {
                return true;
            }
        }
        return false;
    }
    public void juegoTerminado() throws IOException {
        int opc = Integer.parseInt(JOptionPane.showInputDialog(null, "Juego terminado, Perdiste.\n"
                + "Elige Una Opcion: \n"
                + "1: Volver al Menu Principal\n"));

        if (opc > 1 && opc == 0){
            JOptionPane.showMessageDialog(null,"Numero Erroneo");
            juegoTerminado();
        }
        do {
            switch (opc) {
                case 1: {
                    Stage stage = (Stage) contenedor.getScene().getWindow();
                    stage.close();
                }
                break;


            }
        } while (opc < 2);
    }
    public void juegoTerminado2()throws IOException {
        int opc = Integer.parseInt(JOptionPane.showInputDialog(null, "Juego terminado. ¡Has ganado!\n  Tu tiempo fue: " + tiempoCount + " Segundos\n"
              + "Elige Una Opcion: \n"
                + "1: Volver al Menu Principal\n"));

        if (opc > 1 && opc == 0){
            JOptionPane.showMessageDialog(null,"Numero Equivocad, intenta otra vez");
            juegoTerminado();
        }
        do {
            switch (opc) {
                case 1: {
                    Stage stage = (Stage) contenedor.getScene().getWindow();
                    stage.close();
                }
                break;

            }
        } while (opc < 2);
    }
    public void initialize() {

        java.util.Timer tiempoEmpleado = new Timer();
        TimerTask tarea = new TimerTask() {
            @Override
            public void run() {
                tiempoCount++;
            }
        };
        tiempoEmpleado.schedule(tarea, 0, 1000);
        getJ1().setTiempoAcumulado(0);



        listaCasillasPresionadas = new ArrayList<>();
        listaCasillasBandera = new ArrayList<>();

        labelNombre.setText(ControladorInicio.nombre);

        numFilas = ControladorElegirModo.numFila;
        numColumnas = ControladorElegirModo.numcolumna;

        gPaneMatriz = new GridPane();
        gPaneMatriz.setVisible(true);
        gPaneMatriz.setGridLinesVisible(true);
        contenedor.setCenter(gPaneMatriz);
        gPaneMatriz.setMaxHeight(contenedor.getMaxHeight());
        gPaneMatriz.setMaxWidth(contenedor.getMaxWidth());
        gPaneMatriz.setMinHeight(contenedor.getMinHeight());
        gPaneMatriz.setMinWidth(contenedor.getMinWidth());


        for (int i = 1; i <= numFilas; i++) {
            for (int j = 1; j <= numColumnas; j++) {

                Button button = new Button();
                button.setText(" ");
                double btnWidth = button.getWidth();
                double btnHeight = button.getHeight();
                int xI = i;
                int xJ = j;
                Image bandera = new Image(getClass().getResourceAsStream("Bandera.png"));
                Image mina = new Image(getClass().getResourceAsStream("mina.jpg"));
                Label label4 = new Label();


                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        MouseButton button1 = event.getButton();


                        if (button1 == MouseButton.SECONDARY) {

                            button.setPrefWidth(20);
                            button.setMinWidth(20);
                            button.setMaxWidth(20);
                            button.setPrefHeight(25);
                            button.setMinHeight(25);
                            button.setMaxHeight(25);

                            ImageView imageView = new ImageView(bandera);
                            button.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            imageView.setFitWidth(18);
                            imageView.setFitHeight(23);

                            if (button.getGraphic() == null) {
                                button.setGraphic(imageView);
                                listaCasillasBandera.add(String.valueOf(xI) + String.valueOf(xJ));
                            } else {
                                button.setGraphic(null);
                                listaCasillasBandera.remove(String.valueOf(xI) + String.valueOf(xJ));
                            }


                        }
                    }
                });

                button.setOnAction(e -> {


                    if (button.getGraphic() != null) {
                        return;
                    }


                    if (ControladorElegirModo.matrizEnTripletas.existeTripleta(xI, xJ)) {
                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI, xJ).getValor();

                        if (valor == 9) {
                            ImageView imageView = new ImageView(mina);
                            button.setGraphic(imageView);

                            button.setPrefWidth(20);
                            button.setMinWidth(20);
                            button.setMaxWidth(20);
                            button.setPrefHeight(25);
                            button.setMinHeight(25);
                            button.setMaxHeight(25);

                            imageView.setFitWidth(11);
                            imageView.setFitHeight(17);




                            getJ1().setTiempoAcumulado(tiempoCount);

                            try {
                                juegoTerminado();
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }


                        }
                        Label label = new Label("  " + String.valueOf(valor) + "  ");
                        label.setStyle("-fx-font-weight: bold");
                        label.setPrefHeight(25);
                        label.setMaxHeight(25);
                        label.setMinHeight(25);
                        label.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        switch (valor) {
                            case 1:
                                label.setTextFill(Color.BLUE);
                                break;
                            case 2:
                                label.setTextFill(Color.GREEN);
                                break;
                            case 3:
                                label.setTextFill(Color.RED);
                                break;
                            case 4:
                                label.setTextFill(Color.PURPLE);
                                break;
                            case 5:
                                label.setTextFill(Color.DARKRED);
                                break;
                        }
                        gPaneMatriz.add(label, xJ, xI);

                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI));
                        }

                    } else {
                        Label label = new Label("      ");
                        label.setStyle("-fx-font-weight: bold");
                        label.setPrefHeight(25);
                        label.setMaxHeight(25);
                        label.setMinHeight(25);
                        label.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));

                        gPaneMatriz.add(label, xJ, xI);

                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI));
                        }

                    }

                    // Eliminar botones laterales.
                    if (xI != 1 && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI - 1, xJ)) {
                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI - 1, xJ).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ))) {

                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));

                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }


                            gPaneMatriz.add(label1, xJ, xI - 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI - 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI - 1));
                            }

                        }

                    } else if (xI != 1 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));

                        gPaneMatriz.add(label1, xJ, xI - 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI - 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI - 1));
                        }

                    }

                    if (xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI + 1, xJ)) {
                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI + 1, xJ).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ, xI + 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI + 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI + 1));
                            }

                        }

                    } else if (xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ))) {
                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ, xI + 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ) + String.valueOf(xI + 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ) + String.valueOf(xI + 1));
                        }

                    }

                    if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas() && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI, xJ + 1)) {
                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI, xJ + 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI) + String.valueOf(xJ + 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ + 1, xI);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI));
                            }

                        }

                    } else if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas() && !buscarCasilla(listaCasillasBandera, String.valueOf(xI) + String.valueOf(xJ + 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ + 1, xI);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI));
                        }

                    }

                    if (xJ != 1 && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI, xJ - 1)) {
                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI, xJ - 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI) + String.valueOf(xJ - 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ - 1, xI);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI));
                            }

                        }

                    } else if (xJ != 1 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI) + String.valueOf(xJ - 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ - 1, xI);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI));
                        }

                    }

                    if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas()
                            && xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI + 1, xJ + 1)) {

                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI + 1, xJ + 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ + 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ + 1, xI + 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI + 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI + 1));
                            }

                        }

                    } else if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas()
                            && xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ + 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ + 1, xI + 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI + 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI + 1));
                        }

                    }

                    if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas()
                            && xI != 1 && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI - 1, xJ + 1)) {

                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI - 1, xJ + 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ + 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ + 1, xI - 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI - 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI - 1));
                            }

                        }

                    } else if (xJ != ControladorElegirModo.matrizEnTripletas.getNumeroColumnas()
                            && xI != 1 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ + 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ + 1, xI - 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ + 1) + String.valueOf(xI - 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ + 1) + String.valueOf(xI - 1));
                        }

                    }

                    if (xJ != 1
                            && xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI + 1, xJ - 1)) {

                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI + 1, xJ - 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ - 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ - 1, xI + 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI + 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI + 1));
                            }

                        }

                    } else if (xJ != 1
                            && xI != ControladorElegirModo.matrizEnTripletas.getNumeroFilas() && !buscarCasilla(listaCasillasBandera, String.valueOf(xI + 1) + String.valueOf(xJ - 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ - 1, xI + 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI + 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI + 1));
                        }

                    }

                    if (xJ != 1
                            && xI != 1 && ControladorElegirModo.matrizEnTripletas.existeTripleta(xI - 1, xJ - 1)) {

                        int valor = ControladorElegirModo.matrizEnTripletas.getTripleta(xI - 1, xJ - 1).getValor();

                        if (valor != 9 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ - 1))) {
                            Label label1 = new Label("  " + String.valueOf(valor) + "  ");
                            label1.setStyle("-fx-font-weight: bold");
                            label1.setPrefHeight(25);
                            label1.setMaxHeight(25);
                            label1.setMinHeight(25);
                            label1.setBackground(new Background(
                                    new BackgroundFill(
                                            Color.color(
                                                    Color.GRAY.getRed(),
                                                    Color.GRAY.getGreen(),
                                                    Color.GRAY.getBlue(), 0.4d),
                                            new CornerRadii(5),
                                            null)));
                            switch (valor) {
                                case 1:
                                    label1.setTextFill(Color.BLUE);
                                    break;
                                case 2:
                                    label1.setTextFill(Color.GREEN);
                                    break;
                                case 3:
                                    label1.setTextFill(Color.RED);
                                    break;
                                case 4:
                                    label1.setTextFill(Color.PURPLE);
                                    break;
                                case 5:
                                    label1.setTextFill(Color.DARKRED);
                                    break;
                            }
                            gPaneMatriz.add(label1, xJ - 1, xI - 1);
                            if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI - 1))) {
                                listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI - 1));
                            }

                        }

                    } else if (xJ != 1
                            && xI != 1 && !buscarCasilla(listaCasillasBandera, String.valueOf(xI - 1) + String.valueOf(xJ - 1))) {

                        Label label1 = new Label("      ");
                        label1.setStyle("-fx-font-weight: bold");
                        label1.setPrefHeight(25);
                        label1.setMaxHeight(25);
                        label1.setMinHeight(25);
                        label1.setBackground(new Background(
                                new BackgroundFill(
                                        Color.color(
                                                Color.GRAY.getRed(),
                                                Color.GRAY.getGreen(),
                                                Color.GRAY.getBlue(), 0.4d),
                                        new CornerRadii(5),
                                        null)));
                        gPaneMatriz.add(label1, xJ - 1, xI - 1);
                        if (!buscarCasilla(listaCasillasPresionadas, String.valueOf(xJ - 1) + String.valueOf(xI - 1))) {
                            listaCasillasPresionadas.add(String.valueOf(xJ - 1) + String.valueOf(xI - 1));
                        }

                    }

                    System.out.println("Casillas: "+ numCasillas);
                    System.out.println("Casillas presionadas: " + listaCasillasPresionadas.size());
                    if (listaCasillasPresionadas.size() >= numCasillas) {

                        try {
                            juegoTerminado2();
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }

                    }

                });
                gPaneMatriz.add(button, j, i);
            }
        }
    }

}










